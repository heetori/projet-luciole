/*

Squelette de programme d'un bot qui ne fait rien

Il ne vous reste plus qu'à y mettre l'intelligence que vous voulez :)

 */
 
using System;
using BattleIA;

namespace BotRandom
{
    public class TheBot
    {

        // ****************************************************************************************************
        // S'exécute une seule fois au tout début, avant le démarrage du jeu
        // C'est ici qu'il faut initialiser le bot
        public void DoInit()
        {

        } // DoInit

        // ****************************************************************************************************
        /// Réception de la mise à jour des informations du bot
        public void StatusReport(UInt16 turn, UInt16 energy, UInt16 shieldLevel, UInt16 cloakLevel)
        {

        } // StatusReport

        // ****************************************************************************************************
        /// On nous demande la distance de scan que l'on veut effectuer
        public byte GetScanSurface()
        {
            return 0;
        } // GetScanSurface

        // ****************************************************************************************************
        /// Résultat du scan
        public void AreaInformation(byte distance, byte[] informations)
        {

        } // AreaInformation

        // ****************************************************************************************************
        /// On doit effectuer une action
        public byte[] GetAction()
        {
            return BotHelper.ActionNone();
        } // GetAction


    } // TheBot

} // namespace