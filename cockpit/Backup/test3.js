
/*DEBUT AJOUT*/
//decayAllBot(botArray);   //Appel de la fontion pour decay tout les ennemis sur la map
var botArray = []; //tableau vide pour stocker des ennemis plus tard
function CreateBot(id,x,y) { //On va définir un objet bot
  this.id = id; //Un bot a un id
  this.x = x; //Un bot a une coordonnée x
  this.y = y;// un bot a une coordonnée y
  this.numberOfTurnElapsed = 0;//Un bot a un compteur de tour interne depuis sa création
  this.fadeEffectBot = function(){ // même principe que le fadeEffectText mais on se
    // base sur le nombre de tour passé plutot que sur le temps





      var startcolor  = [128,0,128]; // On commence au violet utilisé pour le bot

      if (darktheme){
        endcolor = [35,35,35]; //si on est en darktheme le bot va decay vers le noir utilisé pour le vide
      }
      else{
        endcolor = [255,255,255];//si on n'est pas en darktheme le bot va decay vers le blanc utilisé pour le vide
      }


      var steps       = 8; //On veut qu'au bout de 8 tours le bot ait complètement disparut
      var redChange   = (startcolor[0] - endcolor[0]) / steps; // on vient calculer le pas pour chaque changement de couleur
      var greenChange = (startcolor[1] - endcolor[1]) / steps;
      var blueChange  = (startcolor[2] - endcolor[2]) / steps;

      var currentcolor = startcolor;

      currentcolor[0] = parseInt(currentcolor[0] - redChange * this.numberOfTurnElapsed); // Plus l'ennemi a vécu de tour, plus il va viré vers la couleur d'une case vide
      currentcolor[1] = parseInt(currentcolor[1] - greenChange * this.numberOfTurnElapsed);
      currentcolor[2] = parseInt(currentcolor[2] - blueChange * this.numberOfTurnElapsed);


      //on calcule la position de l'ennemi dans le tableau
      var tr                           = "tr"+(height - this.y)+"";
      var tr                           = document.getElementsByClassName(tr);
      tr[this.x].style.backgroundColor ='rgb(' + currentcolor.toString() + ')' ;
       //on vient changer la couleur actuelle de l'élément par celle calculé maintenant
  }
}

function add_enemi(x,y,height){
  if(map){
    //on souhaite créer un nouvel ennemi,
    //on va lui attribuer comme id, la taill max du tableau pour éviter tout duplicat
    var id = botArray.length;
    var newBot = new CreateBot(id,x,y); // on le créer avec son idées et ses coordonnées
    botArray.push(newBot);   //on va créer une nouvelle instance d'ennemi à chaque appel de cette fonction
    var tr                      = "tr"+(height - y)+"";
    var tr                      = document.getElementsByClassName(tr);
    tr[x].style.backgroundColor = "purple";
  } else {
    console.log("error");
  }
}

function decayAllBot(botArray){
  for (let i = 0; i < botArray.length; i++) { //Pour tout les élements du tableau
    botArray[i].fadeEffectBot();    //on fait decay la couleur de chaque bot un peu plus
    if(botArray[i].numberOfTurnElapsed <=8){
        botArray[i].numberOfTurnElapsed++; //Au augmente le nombre de tours vécu
    }
    else{

    }
  }
}
/*END AJOUT */
