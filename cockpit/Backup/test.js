/*La fonction fadeEffect va permettre de faire une transition lente et agréable d'un
état rgb(x,x,x) vers un autre état rgb(x,x,x)*/
function fadeEffectText(element, startcolor, endcolor) {

  var steps = 32;
  var redChange = (startcolor[0] - endcolor[0]) / steps; // on vient calculer le pas pour chaque changement de couleur
  var greenChange = (startcolor[1] - endcolor[1]) / steps;
  var blueChange = (startcolor[2] - endcolor[2]) / steps;

  var currentcolor = startcolor;
  var stepcount = 0;
  //console.log("1" + startcolor , endcolor);
  var timer = setInterval(function(){   //Toutes les 50 miliseconde on va appeler la fonction suivante
    //console.log(startcolor , endcolor);
     currentcolor[0] = parseInt(currentcolor[0] - redChange);
     currentcolor[1] = parseInt(currentcolor[1] - greenChange);
     currentcolor[2] = parseInt(currentcolor[2] - blueChange);
    // console.log("2" + startcolor , endcolor);
     element.style.color = 'rgb(' + currentcolor.toString() + ')'; //on vient changer la couleur actuelle de l'élément par celle calculé maintenant
     stepcount ++;
     //console.log("3" + startcolor , endcolor);
     if (stepcount >= steps) {  //si on a dépassé le compteur de pas, on vient appliquer la couleur finale
         element.style.color = 'rgb(' + endcolor.toString() + ')';
         //console.log("4" + startcolor , endcolor);

         clearInterval(timer); // on clear l'interval pour éviter une boucle infinie
     }
  }, 50);

}



/*La fonction a pour effet d'alterner le fadeEffect de manière répété et comme couleur rouge -> blanc et blanc -> rouge*/
function blinkingEffectRedText(element , startcolor, endcolor){
  var endTimer=0;
  var isCycleEnded=true; //on va le faire alterner pour passer de blanc à rouge et de rouge à blanc
  var timer = setInterval(function(){//toute les 1700 ms on rejouer cette fonction
    if(isCycleEnded == true){
           fadeEffectText(element, [255,200,200], [255,0,0]);
    }
    else if(isCycleEnded == false){
           fadeEffectText(element,[255,0,0], [255,200,200]);
    }
    endTimer++;
    isCycleEnded = !isCycleEnded;


    if (endTimer > 3){
         clearInterval(timer); // on clear l'interval pour éviter une boucle infinie
    }
  }, 1700);
}



blinkingEffectRedText(document.getElementById("megafaggot"),[255,200,200], [255,0,0]);




var compteur = 0;
var darkmode = false;
function nique(){
  compteur++;
  if (darkmode){
    fadeEffectBot(document.getElementById("walou"),[128,0,128],[0,0,0],compteur);
  }
  else{
    fadeEffectBot(document.getElementById("walou"),[128,0,128],[255,255,255],compteur);
  }

}




function fadeEffectBot(element , startcolor, endcolor,numberOfTurnElapsed){ // même principe que le fadeEffectText mais on se
  // base sur le nombre de tour passé plutot que sur le temps
    var steps = 16;

    var redChange = (startcolor[0] - endcolor[0]) / steps; // on vient calculer le pas pour chaque changement de couleur
    var greenChange = (startcolor[1] - endcolor[1]) / steps;
    var blueChange = (startcolor[2] - endcolor[2]) / steps;

    var currentcolor = startcolor;

    currentcolor[0] = parseInt(currentcolor[0] - redChange * numberOfTurnElapsed);
    currentcolor[1] = parseInt(currentcolor[1] - greenChange * numberOfTurnElapsed);
    currentcolor[2] = parseInt(currentcolor[2] - blueChange *numberOfTurnElapsed);

    element.style.backgroundColor = 'rgb(' + currentcolor.toString() + ')'; //on vient changer la couleur actuelle de l'élément par celle calculé maintenant
}




var audio = new Audio('audio_file.mp3');

//on wrap le audio.play() dans une fonction pour la mettre dans event de button
function music(){
  audio.play();
  //audio.muted = true;
  //audio.playbackRate = 3;
}

function reverse(){
  audio.playbackRate = 1.5;
}




function energyBarFadeIn(){
  if (energy_level > 400 && energy_level < 5000){
    var formula = 230 - (energy_level / 27.7);
    document.getElementById("html_color_energy").style.backgroundColor = 'rgb(13,'+formula.toString() +',13)';
  }
  else if (energy_level >= 5000){
    document.getElementById("html_color_energy").style.backgroundColor = 'rgb(13, 50 ,13)';
  }
}

var energy_level = 400
function updateEnergyBar(){
  energy_level+= 100;
  energyBarFadeIn();
}
/*

230 - (5000 / x) = 50
- (5000 / x) = -180
5000/x = 180
5000 / 180 = x
x = 27.7777777

*/
//rgb 107 232 14
