

//-------- KEY CALLING --------\\
var key_bool = true;
document.addEventListener('keydown', logKey);

//-------- WAITING BOT ---------\\
var wait_bot_bool     = true;
var ipserver          = "localhost";
var guid              = "f5fe3664-cc88-46c5-a613-c771a34cec69";
//var point_active      = true;

//--------- MAP CREATION --------\\
var name              = "";
var width             = "";
var height            = "";
var map_data          = "";
var botArray          = [];
var wall              = "black";

// ------ CHRONOMETRE -----------\\
var setTm       = 0;
var tmStart     = 0;
var tmNow       = 0;
var tmInterv    = 0;
var tTime       = [];
var nTime       = 0;

//------ AUDIO COCKPIT ----------\\
var music               = "";
var music_on            = false;
var music_on_1          = false;
var music_on_2          = false;
var music_on_3          = false;
var music_on_4          = false;
var audio1              = new Audio('audio/audio_file.mp3');
var audio2              = new Audio('audio/audio_file2.mp3');
var audio3              = new Audio('audio/audio_file3.mp3');
var audio4              = new Audio('audio/audio_file4.mp3');

//----- THEME PAR DEFAUT DARK ----\\
if(document.cookie == undefined){
  var darktheme         = true;
  darktheme = document.cookie;
} else {
  darktheme = document.cookie;
}

if(darktheme == "false"){
  darktheme = false;
} else {
  darktheme = true;
}
switchtheme(darktheme);

//----- STRAT THEO BOT -----------\\
var strat_html      = "";
var bool_strat      = true;
var bool_strat_2    = true;
var bool_strat_3    = true;
var bool_strat_4    = true;
var round           = 0;

display_none();
//display_page();
WebSocketTest();


// -------------------------------------------  WEB SOCKET  ------------------------------------------- \\

function WebSocketTest() {

  //On définit les variable nécessaire a la création de la map la transmition des données de fonction en fonction.

  var j             = 0;
  var tour          = 0;
  var x_old         = "";
  var y_old         = "";
  var x_player      = "";
  var y_player      = "";
  var shield_max    = 1;
  var first_round    = true;
  var is_generated  = false;

    //On verifie la compatibilité du navigateur avec les websockets

    if ("WebSocket" in window) {
      console.log("WebSocket is supported by your Browser!");

                 // On ouvre un nouveau un nouveau websockets
                 var ws = new WebSocket("ws://" + ipserver + ":4226/cockpit");
                 // On affiche dans la div'wait' le mot connection pour informer l'utilisateur que nous sommes en train de nous connecter
                 document.getElementById('wait').innerHTML    = "<p id='wait_text'>CONNECTION</p>";
                 document.getElementById('Chrgmt').innerHTML ="<img src='wait.gif'>";
                 document.getElementById('ip_guid').innerHTML = "</br><input type='text' name='ipserver' id='ipserver' value='localhost' /><input type='button' id='button_ip' value='Change IP' onclick='get_ipserv();' />";
                 // On utilise un bool pour ne pas executer la fonction plus de fois qu'il ne faut
                 /*if(wait_bot_bool){
                  // appelle de la fonction wait_bot pour afficher les ...
                  wait_bot();
                  wait_bot_bool = false;
                }*/

                // Quand le websocket s'ouvre on affiche waitting for bot pour informer l'utilisateur que nous sommes connecté au serveur et que nous attendons les bots
                ws.onopen = function() {
                  ws.send(guid);
                  document.getElementById('wait').innerHTML       = "<p id='wait_text'>WAITING FOR BOT</p>";
                  document.getElementById('ip_guid').innerHTML    = "</br><input type='text' name='guid_change' id='guid_change' value='f5fe3664-cc88-46c5-a613-c771a34cec69' /><input type='button' id='button_guid'  value='change GUID' onclick='get_GUID();' />";
                  ws.binaryType                                   = "arraybuffer";
                };

                ws.onmessage = function (evt) {
                  // Quand nous recevons un message nous le stockons dans un tableau data
                  var data        = new Uint8Array(evt.data);

                  switch (data[0]){
                    // 77 correspond a la lettre M permettant de connaitre la hauteur
                    case 77:
                    width         = data[1];
                    height        = data[3];
                    map_data      = data;

                    create_map(height, width);
                    fill_map(data, width, height, wall);

                    break;

                    case 78:
                    // 78 correspond a l lettre N permettant de connaitre le nom du robot
                    for(j=1; j<data.length;j++){
                      name = name + String.fromCodePoint(data[j]);
                    }
                    //display_page(name);

                    break;

                    case 80:
                    //80 correspond a P permettant de connaitre la position x y du robot
                    x_player        = data[1];
                    y_player        = data[2];
                    //first_round est un bouléen permettant de n'executer le code qu'une seul fois au premier tour

                    if(first_round){
                      document.getElementById('wait').innerHTML = "<p id='wait_text'>BOT IS READY</p>";
                      document.getElementById('ip_guid').style.display = 'none';
                      x_old         = x_player;
                      y_old         = y_player;
                      add_bot(x_player,y_player,height);
                      first_round    = false;

                    } else {

                      delet_object(x_old,y_old,height);
                      x_player      = data[1];
                      x_old         = x_player;
                      y_player      = data[2];
                      y_old         = y_player;
                      add_bot(x_player,y_player,height)

                    }
                    decayAllBot(botArray);   //Appel de la fontion pour decay tout

                    break;

                    case 67:
                    // 67 correspond a C permet de connaitre l'energie est de nombreuses informations sur le bot
                    fStart();
                    /*document.getElementById('bot_ready').style.display  = 'none';
                    document.getElementById('game').style.display       = 'inline';
                    */
                    display_page(name);

                    if(music_on){
                      start_music(first_round);
                    }

                    tour++;
                    var energie_player        = data[1]+data[2]*256;
                    var shield_player         = data[3]+data[4]*256;
                    var invisibility_player   = data[5]+data[6]*256;
                    var score_player          = data[7]+data[8]*256;

                    info(energie_player,shield_player,invisibility_player,score_player,tour, shield_max);
                    //strat(energie_player,shield_player,invisibility_player, tour);
                    //document.getElementById( 'change_theme' ).style.display = 'none';
                    if(first_round){
                      first_round = false;
                    }

                    break;

                    case 73:

                    //73 correponds a la lettre  I permettant de connaitre le scan de la map

                    var diam = data[1];
                    update_map(diam, data, width, height, x_player, y_player, first_round);

                    break;

                    case 68:
                    //68 correpond a D cela veut dire que le bot est mort

                    fStop();
                    /*document.getElementById( 'page' ).style.display = 'none';
                    var rndgif = Math.floor(Math.random() * 9) + 1;
                    document.getElementById( 'dead_bot' ).innerHTML = "<img src='dead_bot/dead_bot"+rndgif+".gif' style='width:400px; height: 400px;'><br><p>Le bot est mort :( </p>";
                    */
                    break;

                    default:
                  }
                };

                ws.onclose = function() {
                    // websocket is closed.
                    console.log("Connection is closed...");
                  };
                } else {

                 // The browser doesn't support WebSocket
                 console.log("WebSocket NOT supported by your Browser!");
               }
             }


  // ----------------------------------------------------------------------------------------------------------- \\

  // ------------------------------------------------ DISPLAY --------------------------------------------------- \\

  function display_page(name){
    document.getElementById('party').style.display               = 'inline';
    document.getElementById('wait').style.height                   = '0px';
    document.getElementById('wait').style.display                   = 'none';
    document.getElementById('Chrgmt').style.display                   = 'none';
    //document.getElementById('ip_guid').style.display              ='none';
    document.getElementById('wait').style.display                   = 'none';
    document.getElementById('Settings').style.height             ='0';
   /* document.getElementById('page').style.display               = 'inline';
    document.getElementById('point').style.display              = 'none';
    document.getElementById('setting').style.float              = 'right';*/
    document.getElementById("name_bot").innerHTML             = name;
    /*document.getElementById('ip_guid').style.display            = 'none';
    document.getElementById('setting').style.display            = 'inline';
    document.getElementById('Etat_tab').style.display           = 'inline';
    document.getElementById('Score_Tour').style.display         = 'inline';*/
  }

  function display_none() {
    document.getElementById('party').style.display               = 'none';
    /*document.getElementById('setting').style.float              = 'none';
    document.getElementById('music1').style.display             = 'none';
    document.getElementById('music2').style.display             = 'none';
    document.getElementById('music3').style.display             = 'none';
    document.getElementById('music4').style.display             = 'none';
    document.getElementById( 'page' ).style.display             = 'none';
    document.getElementById('Etat_tab').style.display           = 'none';
    document.getElementById('Score_Tour').style.display         = 'none';*/
  }



  // ----------------------------------------------------------------------------------------------------------- \\

  // -------------------------------------  CREATION ET EDITION DE LA MAP --------------------------------------- \\

  /*create_map() va générer le tableau de la map (utilisée une seul fois)*/
  function create_map(height, width){

    var i           = 0;
    var map         = false;
    var creatmap    = "";
    var title       = "<table id='table_map'>";  //
    var creatmap1   = "<tr>";     //
    var creatmap3   = "</td>";    // Ces variables servent à construire le tableau en html
    var creatmap4   = "</tr>";    //

      //Création du tableau de la carte
      for(i=1; i <= height; i ++){
        creatmap = creatmap + creatmap1;
        for(j=1; j <= width; j ++){
          var creatmap2 = "<td class=tr"+i+">";
          creatmap = creatmap + creatmap2+creatmap3;
        }
        creatmap = creatmap + creatmap4;
      }
      document.getElementById("map").innerHTML = title+creatmap+"</table>";
      map = true; //A la fin de génération, on set la var map à true
    }

    /*le fill_map() va remplir la map avec les murs (utilisée qu'un fois)*/
    function fill_map(data, width,height, wall){

                                                    //le tableau data est le champ "Mwh#######" renvoyé par le serveur
      var k = 5;                                    //k est le curseur du tableau data (on commence au 5ème octet, c'est le premier octect contenant le terrain)

                                                    //On parcours l'intégralité du champ data pour l'appliquer sur le tableau
      for(i=height;i>0 ;i--){                       //On fait commencer i à height et non à 0 pour avoir une map comme sur Unity
       for(j=0;j<width;j++){
        var str = data[k];
        if(str == 2){
          if(map){
            add_wall(j,i, wall);
          } else {
            console.log("error");
          }
        }
        k++;
      }
    }
  }

  // ----------------------------------------------------------------------------------------------------------- \\

  // ---------------------------------------- UPDATE MAP & FUNCTION --------------------------------------------- \\

  function update_map(diam, data, width, height, x_player, y_player, first_tour){
    var rayon         = (diam - 1)/2;
    var x_start       = x_player - rayon;
    var y_start       = y_player + rayon;
    var x_start_save  = x_start;
    k                 = 2;

    for(j = 0; j <diam; j++) {
      for(i=0; i <diam  ; i++){
        var str = data[k];

        if(str == 0){                                           //Cas où 0, on place du vide delet_object
          if(x_start<width){
            delet_object(x_start,y_start,height);
          }
        }

        if(str == 3){                                           //Cas où 3, on place de l'énergie
          if(x_start < width){
            add_energie(x_start,y_start,height);
          }else{
            console.log("error");
          }
        }
                                                                //Le server renvoi 4 pour le joueur ET les ennemis
        if(str == 4){                                           //Cas où 4, on place un ennemi si seulement si
          if(x_player != x_start && y_start != y_player){       //les coordonnées ne sont pas égales à celle du joueur
            add_enemi(x_start,y_start,height);
          }
        }
        k++;
        x_start++;
      }
      x_start = x_start_save;
      y_start--;
    }
  }

  function CreateBot(id,x,y) {                              //On va définir un objet bot
    this.id                   = id;                         //Un bot a un id
    this.x                    = x;                          //Un bot a une coordonnée x
    this.y                    = y;                          // un bot a une coordonnée y
    this.numberOfTurnElapsed  = 0;                          //Un bot a un compteur de tour interne depuis sa création
    this.fadeEffectBot        = function(){                 // même principe que le fadeEffectText mais on se
                                                            // base sur le nombre de tour passé plutot que sur le temps
        var startcolor        = [128,0,128];                // On commence au violet utilisé pour le bot

        if (darktheme){
          endcolor            = [35,35,35];                 //si on est en darktheme le bot va decay vers le noir utilisé pour le vide
        }
        else{
          endcolor            = [255,255,255];              //si on n'est pas en darktheme le bot va decay vers le blanc utilisé pour le vide
        }

        var steps             = 16;                          //On veut qu'au bout de 8 tours le bot ait complètement disparut
        var redChange         = (startcolor[0] - endcolor[0]) / steps; // on vient calculer le pas pour chaque changement de couleur
        var greenChange       = (startcolor[1] - endcolor[1]) / steps;
        var blueChange        = (startcolor[2] - endcolor[2]) / steps;

        var currentcolor      = startcolor;

        currentcolor[0]       = parseInt(currentcolor[0] - redChange    * this.numberOfTurnElapsed); // Plus l'ennemi a vécu de tour, plus il va viré vers la couleur d'une case vide
        currentcolor[1]       = parseInt(currentcolor[1] - greenChange  * this.numberOfTurnElapsed);
        currentcolor[2]       = parseInt(currentcolor[2] - blueChange   * this.numberOfTurnElapsed);


        //on calcule la position de l'ennemi dans le tableau
        var tr                           = "tr"+(height - this.y)+"";
        var tr                           = document.getElementsByClassName(tr);
        tr[this.x].style.backgroundColor ='rgb(' + currentcolor.toString() + ')' ;
         //on vient changer la couleur actuelle de l'élément par celle calculé maintenant
       }
     }


     function add_wall(x,y, wall){     //cette fonction ajoute un mur
      var tr                      = "tr"+(y)+"";
      var tr                      = document.getElementsByClassName(tr);
      tr[x].style.backgroundColor = wall;
      tr[x].style.boxShadow       = "2px 2px 10px gray inset";
      tr[x].style.webkitTransform ="2px 2px 10px gray inset"
    }


    function add_energie(x,y,height){   //cette fonction ajoute une cellule d'énergie
    if(map){
      var tr                      = "tr"+(height - y)+"";
      tr                          = document.getElementsByClassName(tr);
      tr[x].style.backgroundColor = "yellow";
    } else {
      console.log("error");
    }
  }


  function add_bot(x,y,height){          //Cette fonction ajoute le joueur
    if(map){
      var tr                      = "tr"+(height - y)+"";
      var tr                      = document.getElementsByClassName(tr);
      tr[x].style.backgroundColor = "red";
    } else {
      console.log("error");
    }
  }


  function add_enemi(x,y,height){
    if(map){
                                          //on souhaite créer un nouvel ennemi,
                                          //on va lui attribuer comme id, la taill max du tableau pour éviter tout duplicat
                                          var id = botArray.length;
      var newBot = new CreateBot(id,x,y); // on le créer avec son idées et ses coordonnées
      botArray.push(newBot);              //on va créer une nouvelle instance d'ennemi à chaque appel de cette fonction
      var tr                      = "tr"+(height - y)+"";
      var tr                      = document.getElementsByClassName(tr);
      tr[x].style.backgroundColor = "purple";
    } else {
      console.log("error");
    }
  }

  function energyBarFadeIn(energy_level){                     //Cette fonction fait virer la bar d'énergie du vert clair au vert foncé sur un intervalle de 40 à 5000 d'énergie
    if (energy_level > 40 && energy_level < 400){            //si 40 < energy_level < 5000
      var formula = 230 - (energy_level / (400/180));              //La formule calculé pour le niveau de vert 230 et la borne supérieur 5000
      document.getElementById("progress_bar").style.backgroundColor = 'rgb(13,'+formula.toString() +',13)';
    }
    else if (energy_level >= 300){                           // Si on dépasse 5000 on va mettre une fixed value pour la couleur de la bar d'énergie
      document.getElementById("progress_bar").style.backgroundColor = 'rgb(13, 50 ,13)';
  }
}



  function delet_object(x,y,height){   // Cette fonction vient effacer l'objet placé en x,y
  if(map){
    var tr                        = "tr"+(height - y)+"";
    var tr                        = document.getElementsByClassName(tr);

      tr[x].style.backgroundColor = "rgba(0,0,0,0)";   //On vient mettre de une opacité de 0 pour que simplifier le darktheme et le lighttheme
    } else {
      console.log("error");
    }
  }

  function decayAllBot(botArray){                     // cette fonction vient augmenter et gérer la couleur des ennemis scannés récemment
    for (var i = 0; i < botArray.length; i++) {       //Pour tout les élements du tableau
      if(botArray[i].numberOfTurnElapsed <= 16){       //On vient faire decay le bot sur 8 tours seulement
            botArray[i].fadeEffectBot();              //on fait decay la couleur de chaque bot un peu plus
            botArray[i].numberOfTurnElapsed++;        //On augmente le nombre de tours vécu
          }

        }
      }

  // ------------------------------------------------------------------------------------------------------------ \\


  // ----------------------------- CREATION ET EDITION DU TABLEAU DES INFORMATIONS ------------------------------ \\

  function info (Energie, shield, invisibility, Score, tour, shield_max){//cette fonction vient rendre dynamic la couleur et le texte de
     // l'énergie, le shield et l'invisibilité
     document.getElementById("Round_info").innerHTML                        = tour;
    if(Energie < 40){ //si le niveau d'énergie est en dessous de 40 (seuil arbitrairement critique)
      document.getElementById("Energy_bar_inside").innerHTML                     = "<p id='text_bar'>LOW ENERGY LEVEL</p>";//On modifie le text
    document.getElementById("Energy").innerHTML        = Energie;
      document.getElementById("Energy_bar").style.backgroundColor         = "rgba(199,0,0,0.8)"; //On modifie la couleur
    } else {
      document.getElementById("Energy_bar_inside").innerHTML                     = "<p id='text_bar'>ENERGY LEVEL</p>";
      document.getElementById("Energy").innerHTML        = Energie;
    document.getElementById("Energy_bar").style.backgroundColor         = "rgba(107,232,14,0.8)"; //On modifie la couleur
  }

    if(Energie < 250){      //On va venir la taille de la div en fonction de l'énergie, 400 d'énergie vaut 100% (arbitraire encore une fois)
      var energie_prct = Energie/250*100;
    document.getElementById("Energy_bar_inside").style.height                   = 100-energie_prct+"%";
  } else {
    document.getElementById("Energy_bar_inside").style.height                   = "0%";
    //energyBarFadeIn(Energie);
  }

    if(shield < 15 ) { //Même raisonnement mais pour le shield
      document.getElementById("Shield_bar_inside").innerHTML              = "<p id='text_bar'>NO SHIELD</p>";
      //document.getElementById("progress_bar_shield").style.backgroundColor  = "rgb(199,0,0)";
    } else {
      document.getElementById("Shield_bar_inside").innerHTML              = "<p id='text_bar'>SHIELD LEVEL</p>";
      //document.getElementById("progress_bar_shield").style.backgroundColor  = "rgb(134,224,225)";
    }

    if(shield > shield_max ){ // au lieu d'une valeur fixée comme pour l'énergie, 100% représente la valeur  shield incantée par le joueur
      shield_max = shield;
  }
  var shield_prct = shield/shield_max*100;
  document.getElementById("Shield_bar_inside").style.height              = 100-shield_prct+"%";
  document.getElementById("Shield").innerHTML        = shield;
  document.getElementById("Cloak_bar_inside").innerHTML                       = "<p id='text_bar'>CLOAK LEVEL</p>"; //On refresh la valeur de l'invisibilité
  document.getElementById("Cloak").innerHTML                               = invisibility; //On refresh la valeur de l'invisibilité

        document.getElementById("Score_info").innerHTML                              = Score ; // On refresh la valeur du score
      }


  // ----------------------------------------------------------------------------------------------------------- \\

  // -------------------------------------------------- THEME --------------------------------------------------- \\

  function theme(){
    var checkBox = document.getElementById("theme");
    if (checkBox.checked == true){
      darktheme = false;
      switchtheme(darktheme);
    }else {
      darktheme = true;
      switchtheme(darktheme);
    }
    document.cookie = darktheme;
  }

  function switchtheme (darktheme){
    switch (darktheme){

      case true:

      wall                                                        = "white";
      var background                                              = "rbg(35,35,35)"
      document.body.style.backgroundColor                         = "rgb(35,35,35)";
      document.getElementById("wall_Color").style.backgroundColor  = "white";
      document.body.style.color                                   = "white";
      document.getElementById('mySidenav').style.backgroundColor  = 'white';
      document.getElementById('Energy_bar_inside').style.backgroundColor  = 'rgb(35,35,35)';
      document.getElementById('Shield_bar_inside').style.backgroundColor  = 'rgb(35,35,35)';
      document.getElementById('Cloak_bar_inside').style.backgroundColor  = 'rgb(35,35,35)';
      document.getElementById('mySidenav').style.color  = 'black';
      document.getElementById('setting').style.color              = "lightgrey";
      document.getElementById('setting').style.borderColor        = "lightgrey";
      document.getElementById('Chrgmt').style.filter              = "invert(86.2%)";
      document.getElementById('setting').style.filter              = "invert(1)";
      fill_map(map_data, width, height, wall);

      break;

      case false:
      wall                                                        = "black";
      document.body.style.color                                   = "black";
      document.body.style.backgroundColor                         = "white";
      document.getElementById('Energy_bar_inside').style.backgroundColor  = 'white';
      document.getElementById('Shield_bar_inside').style.backgroundColor  = 'white';
      document.getElementById('Cloak_bar_inside').style.backgroundColor  = 'white';
      document.getElementById("wall_Color").style.backgroundColor  = "black";
      document.getElementById('mySidenav').style.backgroundColor  = '#111';
      document.getElementById('mySidenav').style.color  = 'white';
      document.getElementById('setting').style.color              = 'black';
      document.getElementById('setting').style.borderColor        = 'black';
      document.getElementById('Chrgmt').style.filter              = "invert(0)";
      document.getElementById('setting').style.filter              = "invert(0)";
      fill_map(map_data, width, height, wall);

      break;

      default:
    }
  }

  // ----------------------------------------------------------------------------------------------------------- \\

  // -------------------------------------------------- WAIT BOT ----------------------------------------------- \\

  function wait_bot(){
    var isCycleEnded=true;                          //on va le faire alterner pour passer de blanc à rouge et de rouge à blanc
    var timer = setInterval(function(){             //toute les 1700 ms on rejouer cette fonction
      if(isCycleEnded == true){
        setTimeout(function() {
          document.getElementById("point").innerHTML = ' ';}, 0);
        setTimeout(function() {
          document.getElementById("point").innerHTML = '.';}, 500);
        setTimeout(function() {
          document.getElementById("point").innerHTML = '..';}, 1000);
        setTimeout(function() {
          document.getElementById("point").innerHTML = '...';}, 1500);
      }
      else if(isCycleEnded == false){
        setTimeout(function() {
          document.getElementById("point").innerHTML = ' ';}, 0);
        setTimeout(function() {
          document.getElementById("point").innerHTML = '.';}, 500);
        setTimeout(function() {
          document.getElementById("point").innerHTML = '..';}, 1000);
        setTimeout(function() {
          document.getElementById("point").innerHTML = '...';}, 1500);
      }
      isCycleEnded = !isCycleEnded;
    }, 2000);
  }
  // ----------------------------------------------------------------------------------------------------------- \\

  // ---------------------------------------------- GET IP & GUID ----------------------------------------------- \\

  function get_ipserv(){
   ipserver = document.getElementById("ipserver").value;
   WebSocketTest();
 }

 function get_GUID(){
   guid = document.getElementById("guid_change").value;
   WebSocketTest();
 }

  // ----------------------------------------------------------------------------------------------------------- \\

  // ---------------------------------------------- CHRONOMETRE ------------------------------------------------- \\

  function affTime(tm){ //affichage du compteur
   var vMin       = tm.getMinutes();
   var vSec       = tm.getSeconds();
     var vMil     = Math.round((tm.getMilliseconds())/10); //arrondi au centième
     if (vMin<10){
      vMin        = "0"+vMin;
    }
    if (vSec<10){
      vSec        = "0"+vSec;
    }
    if (vMil<10){
      vMil        = "0"+vMil;
    }
    document.getElementById("Time_info").innerHTML=vMin+":"+vSec+":"+vMil;
  }

  function fChrono(){
   tmNow          = new Date();
   Interv         = tmNow-tmStart;
   tmInterv       = new Date(Interv);
   affTime(tmInterv);
 }

 function fStart(){
   fStop();
   if (tmInterv==0) {
    tmStart     = new Date();
     } else {                           //si on repart après un clic sur Stop
      tmNow     = new Date();
      Pause     = tmNow-tmInterv;
      tmStart   = new Date(Pause);
    }
     setTm=setInterval(fChrono,10);     //lancement du chrono tous les centièmes de secondes
   }

   function fStop(){
     clearInterval(setTm);
     tTime[nTime]=tmInterv;
   }
  // ------------------------------------------------------------------------------------------------------------ \\

  // ------------------------------------------ GESTION DE LA MUSIQUE -------------------------------------------\\

  // DEBUT AJOUT

  function NewMusic(audioPath){

    this.isOn  = false;

    this.audio = new Audio(audioPath);

  }

  var musicArray = [];
  var numberOfMusicAvailable = 3;

  for(var i = 0; i<numberOfMusicAvailable;i++){
    var addNewMusic = "audio/audio_file"+i+".mp3";
    var music = new NewMusic(addNewMusic);
    musicArray.push(music);
  }


    function startMusic(musicId){
      musicArray[musicId].isOn = true;
      musicArray[musicId].audio.play();
    }

    function pauseMusic(musicId){
      musicArray[musicId].isOn = false;
      musicArray[musicId].audio.paused = true;
    }

    function muteMusic(musicId){
      musicArray[musicId].audio.muted =  !(musicArray[musicId].audio.muted);
    }


    function loopMusic(musicId){
      musicArray[musicId].audio.loop;
    }

  //FIN AJOUT

  // ------------------------------------------------------------------------------------------------------------ \\

  // --------------------------------- GESTION DE L'AFFICHAGE DE LA STRATEGIE ------------------------------------\\

  function strat(Energie, Shield, cloak, tour){

    if(tour == 1){
      round++;
      strat_html                                                = "<tr><td id='round_"+round+"'>GOT TO 20 CLOAK 0 SHIELD</td></tr>";
      document.getElementById('strat').innerHTML                = strat_html;
      document.getElementById("round_"+round).style.borderColor = "yellow";
      document.getElementById('Gun').style.backgroundColor      = "red";

    } else if(tour >= 2 && Energie < 150 && bool_strat){
      document.getElementById("round_"+round).style.borderColor = "gray";
      round++;
      strat_html                                                = "<tr><td id='round_"+round+"'>STAY AT 20 OF CLOAK 0 OF SHIELD AND RUSH ENERGY</td></tr>" +strat_html;
      document.getElementById('strat').innerHTML                = strat_html;
      document.getElementById("round_"+round).style.borderColor = "yellow";
      bool_strat                                                = false;
      document.getElementById('Gun').style.backgroundColor      = "red";
    } else if(tour >= 2 && Energie >= 150 && bool_strat_2){
      // BOOLEEN 2
      document.getElementById("round_"+round).style.borderColor = "gray";
      round++;
      strat_html                                                = "<tr><td id='round_"+round+"'>CHANGE 20 CLOAK IN 20 SHIELD AND FIRE ON</td></tr>" +strat_html;
      document.getElementById('strat').innerHTML                = strat_html;
      document.getElementById("round_"+round).style.borderColor = "yellow";
      document.getElementById('Gun').style.backgroundColor      = "green"; // rbg(107,232,14)
      bool_strat_2                                              = false;
      bool_strat_3                                              = true;
      bool_strat_4                                              = true;
    } else if(tour >= 2 && Energie >= 200 && bool_strat_3){
      // BOOLEEN 3
      document.getElementById("round_"+round).style.borderColor = "gray";
      round++;
      strat_html                                                = "<tr><td id='round_"+round+"'>UP TO 50 SHIELD STAY AT 0 CLOAK</td></tr>" +strat_html;
      document.getElementById('strat').innerHTML                = strat_html;
      document.getElementById("round_"+round).style.borderColor = "yellow";
      document.getElementById('Gun').style.backgroundColor      = "rbg(107,232,14)";
      bool_strat_2                                              = false;
      bool_strat_3                                              = false;
      bool_strat_4                                              = true;
    } else if(tour >=2 && Energie <= 75 && bool_strat_4){
      // BOOLEEN 4
      document.getElementById("round_"+round).style.borderColor = "gray";
      round++;
      strat_html                                                = "<tr><td id='round_"+round+"'>CHANGE FULL SHIELD IN CLOAK STOP FIRE AND RUSH ENERGY</td></tr>" +strat_html;
      document.getElementById('strat').innerHTML                = strat_html;
      document.getElementById("round_"+round).style.borderColor = "yellow";
      document.getElementById('Gun').style.backgroundColor      = "red";
      bool_strat_2                                              = true;
      bool_strat_3                                              = true;
      bool_strat_4                                              = false;
    }
  }



function openNav() { // Cette fonction permet d'ouvrir la bar latéral des settings
document.getElementById("mySidenav").style.width = "100%";
}

function closeNav() { //Cette fonction vient fermer la bar latéral des settings
  document.getElementById("mySidenav").style.width = "0%";
}

function logKey(e) {
  if(e.code == "Escape" && key_bool){
    openNav();
    key_bool = false;
  } else if(e.code == "Escape" && !key_bool){
    closeNav();
    key_bool = true;
  } else if(e.code == 'Tab'){
    console.log(musicArray[2]);
    startMusic(2);
  }
}
